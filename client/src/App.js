import React, {Component} from 'react';
import HomePage from "./component/HomePage";
import {Route, BrowserRouter as Router} from "react-router-dom";
import SaveImage from "./component/SaveImage";
import RegisterOrLoginPage from "./component/RegisterOrLoginPage";

class App extends Component {


    render() {
        return (
            <>
                <Router>
                        <Route exact path={'/'} component={HomePage}/>
                        <Route exact path={'/home'} component={HomePage}/>
                        <Route exact path={'/myies'} component={SaveImage}/>
                        <Route exact path={'/login'} component={RegisterOrLoginPage}/>
                </Router>
            </>
        );
    }
}

export default App;