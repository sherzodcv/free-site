package com.com.repository;

import com.com.entity.Extralesson;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;


public interface ExtraLessonRepository extends JpaRepository<Extralesson, UUID> {
    List<Extralesson> findAllByLessonId(UUID lesson_id);
}
