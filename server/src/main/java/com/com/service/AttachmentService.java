package com.com.service;

import com.com.entity.Attachment;
import com.com.entity.AttachmentContent;
import com.com.exceptions.ResourceNotFound;
import com.com.payload.ApiResponse;
import com.com.repository.AttachmentContentRepository;
import com.com.repository.AttachmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Service
public class AttachmentService {

    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    AttachmentContentRepository attachmentContentRepository;


    public ApiResponse getAll(List<UUID> attachmentIds) {
        try {
            List<AttachmentContent> allById = attachmentContentRepository.findAllById(attachmentIds);
            return new ApiResponse("success",true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ApiResponse("error",false);
    }


    public ApiResponse saveFile(MultipartFile request) throws IOException {
        Attachment attachment = attachmentRepository.save(new Attachment(request.getName(), request.getContentType(), request.getSize()));
        attachmentContentRepository.save(new AttachmentContent(request.getBytes(),attachment));
        return new ApiResponse("success", true, attachment);
    }
    public HttpEntity<?> getAttachmentContent(UUID attachmentId, HttpServletResponse response) {
        Attachment attachment = attachmentRepository.findById(attachmentId).orElseThrow(() -> new ResourceNotFound("Attachment", "id", attachmentId));
        AttachmentContent attachmentContent = attachmentContentRepository.findByAttachment(attachment).orElseThrow(() -> new ResourceNotFound("Attachment content", "attachment id", attachmentId));
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(attachment.getContentType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + attachment.getName() + "\"")
                .body(attachmentContent.getBytes());
    }

    public ApiResponse deleteFile(UUID id) {
        try {
            attachmentContentRepository.deleteById(id);
            return new ApiResponse("success",true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ApiResponse("error",false);
    }

    public ApiResponse editFile(MultipartHttpServletRequest request) {
        return null;
    }
}
