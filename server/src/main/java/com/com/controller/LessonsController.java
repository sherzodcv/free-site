package com.com.controller;

import com.com.entity.Extralesson;
import com.com.repository.ExtraLessonRepository;
import com.com.repository.LessonRepository;
import com.com.service.LessonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/lessons")
public class LessonsController {


    @Autowired
    LessonRepository lessonRepository;

    @Autowired
    ExtraLessonRepository extraLessonRepository;

    @Autowired
    LessonService lessonService;

    @GetMapping
    public HttpEntity<?> getAll() {
        return ResponseEntity.ok(lessonRepository.findAll());
    }

    @GetMapping("/one")
    public HttpEntity<?> getOne(@RequestParam UUID id) {
        return ResponseEntity.ok(lessonRepository.findById(id));
    }

    @GetMapping("/extra")
    public HttpEntity<?> getExtraLessons(@RequestParam UUID lessonId) {
        List<Extralesson> extraLessons = extraLessonRepository.findAllByLessonId(lessonId);
        return ResponseEntity.ok(extraLessons);
    }
}
