package com.com.controller;

import com.com.repository.ExtraLessonRepository;
import com.com.service.ExtraLessonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.security.auth.login.CredentialException;
import java.util.UUID;

@RestController
@RequestMapping("/extra")
public class ExtraLessonController {

    @Autowired
    ExtraLessonRepository extraLessonRepository;

    @Autowired
    ExtraLessonService extraLessonService;

    @GetMapping
    public HttpEntity<?> getAll(){
        return ResponseEntity.ok(extraLessonRepository.findAll());
    }
    @GetMapping("/one")
    public HttpEntity<?> getOne(@RequestParam UUID id){
        return ResponseEntity.ok(extraLessonRepository.findById(id));
    }
}
