package com.com.component;

import com.com.entity.Role;
import com.com.entity.User;
import com.com.repository.RoleRepository;
import com.com.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collections;

import static com.com.entity.enums.RoleName.ROLE_SUPER_ADMIN;
import static com.com.entity.enums.RoleName.ROLE_USER;


@Component
public class DataLoader implements CommandLineRunner {

    @Value("${spring.datasource.initialization-mode}")
    private String initialMode;


    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {
        if (initialMode.equals("always")) {
            roleRepository.save(new Role(1, ROLE_SUPER_ADMIN));
            roleRepository.save(new Role(2, ROLE_USER));
            userRepository.save(new User(
                    "Sherzod",
                    "G'ulomqodirov",
                    "+998999113181",
                    passwordEncoder.encode("1237"),
                    roleRepository.findAllByNameIn(
                            Collections.singletonList(ROLE_SUPER_ADMIN))));

        }
    }

}
