package com.com.entity;

import com.com.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.util.List;

import static javax.persistence.FetchType.LAZY;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity

public class Lesson  extends AbsEntity {

    @Column(nullable = false)
    private String name;

    private String code;

    private String text;

    @ManyToMany(fetch = LAZY)
    @JoinTable(name = "lesson_image", joinColumns = {@JoinColumn(name = "lesson_id")},
            inverseJoinColumns = {@JoinColumn(name = "attachment_id")})
    private List<Attachment> images;
}
