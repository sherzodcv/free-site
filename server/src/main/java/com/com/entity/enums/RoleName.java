package com.com.entity.enums;

public enum RoleName {

    ROLE_SUPER_ADMIN,
    ROLE_USER

}
