package com.com.entity;

import com.com.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AttachmentContent extends AbsEntity {

    private byte[] bytes;

    @OneToOne(fetch = FetchType.LAZY)
    private Attachment attachment;
}
