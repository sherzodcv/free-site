package com.com.payload;

import lombok.Data;

import java.util.UUID;

@Data
public class ReqUser {

    private UUID id;

    private String firstName;

    private String lastName;

    private String password;

    private String phoneNumber;
}
