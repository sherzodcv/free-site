package com.com.payload;

import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class ReqLesson {

    private UUID id;

    private String name;

    private String code;

    private String text;

    private List<UUID> imagesId;

}
